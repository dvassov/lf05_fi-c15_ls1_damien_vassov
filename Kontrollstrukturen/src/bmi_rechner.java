import java.util.Scanner;
public class bmi_rechner {
public static void main(String [] args) {
	Scanner myScanner = new Scanner(System.in);
	double gewicht = 0d;
	double groesse = 0d;
	double bmi = 0;
	char geschlecht = 0;
	
	System.out.print("Gebe dein K�rpergewicht in kg an: ");
	gewicht = eingabeGewicht(myScanner, gewicht);
	System.out.print("Gebe deine K�rpergr��e in m an: ");
	groesse = eingabeGroesse(myScanner, groesse);
	System.out.print("Gebe dein Geschlecht an (m/w): ");
	geschlecht = eingabeGeschlecht(myScanner, geschlecht);
	
	bmi = berechnungBMI(groesse, gewicht);
	
	
	if(geschlecht == 'm') {
		bmiMaennlich(bmi);
	}
	else if(geschlecht == 'w') {
		bmiWeiblich(bmi);
	}
	else
		System.out.print("\n Fehler! \n\nBitte gebe dein Geschlecht an \n w = Weiblich \n m = M�nnlich");
	
	
	

}
public static double eingabeGewicht(Scanner myScanner, double gewicht) {
	return myScanner.nextDouble();
}
public static double eingabeGroesse(Scanner myScanner, double groesse) {
	return myScanner.nextDouble();
}
public static char eingabeGeschlecht(Scanner myScanner, char geschlecht) {
	return myScanner.next().charAt(0);
}
public static double berechnungBMI(double groesse, double gewicht) {
	return (gewicht / (groesse * groesse));
}
public static void bmiMaennlich(double bmi) {
	if(bmi <20) {
		System.out.print("Dein BMI betr�gt: " + bmi + ". Du bist untergewichtig");
	}
	else if((bmi >= 20) && (bmi <= 25)) {
		System.out.print("Dein BMI betr�gt: " + bmi + ". Du liegst im Normalgewicht.");
		
	}
	else if(bmi >25) {
		System.out.print("Dein BMI betr�gt: " + bmi + ". Du bist �bergewichtig.");
	}
}
public static void bmiWeiblich(double bmi) {
	if(bmi <19) {
		System.out.print("Dein BMI betr�gt: " + bmi + ". Du bist untergewichtig");
	}
	else if((bmi >= 19) && (bmi <= 24)) {
		System.out.print("Dein BMI betr�gt: " + bmi + ". Du liegst im Normalgewicht.");
		
	}
	else if(bmi >24) {
		System.out.print("Dein BMI betr�gt: " + bmi + ". Du bist �bergewichtig.");
	}

}
}

