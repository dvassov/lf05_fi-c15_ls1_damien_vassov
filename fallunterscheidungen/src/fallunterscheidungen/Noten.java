package fallunterscheidungen;

import java.util.Scanner;

public class Noten {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int note = 0;
		System.out.println("Gebe deine Note ein: ");
		note = scan.nextInt();
		vergleich(note);
	}


	public static void vergleich(int note) {
		switch (note) {
		case 1:
			System.out.println("Die Note " + note + " entspricht: Sehr Gut");
			break;
		case 2:
			System.out.println("Die Note " + note + " entspricht: Gut");
			break;
		case 3:
			System.out.println("Die Note " + note + " entspricht: Befriedigend");
			break;
		case 4:
			System.out.println("Die Note " + note + " entspricht: Ausreichend");
			break;
		case 5:
			System.out.print("Die Note " + note + " entspricht: Mangelhaft");
			break;
		case 6:
			System.out.println("Die Note " + note + " entspricht: Ungenügend");
			break;
		default:
			System.out.println(note + " Liegt nicht zwischen 1 - 6");
		}
	}
}
