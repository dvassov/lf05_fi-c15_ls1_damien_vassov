
public class volumenRechnung {
	public static void main (String [] args) {
		System.out.print("Volumenberechnung\n\n");
		int wuerfelA = 4;
		int quaderA = 5;
		int quaderB = 10;
		int quaderC = 8;
		int pyramideA = 7;
		int pyramideH = 5;
		int kugelR = 3;
		double pi = 3.14159265359;
		wuerfel(wuerfelA);
		quader(quaderA, quaderB, quaderC);
		pyramide(pyramideA, pyramideH);
		kugel(kugelR, pi);
	}
	
	public static void wuerfel(int wuerfelA) {
	System.out.print("Volumen des W�rfels betr�gt: ");
	System.out.println(wuerfelA * wuerfelA * wuerfelA + " cm�");
	}
	public static void quader(int quaderA, int quaderB, int quaderC) {
		System.out.print("Volumen des Quaders betr�gt: ");
		System.out.println(quaderA * quaderB * quaderC + " cm�");
		
	}
	public static void pyramide(int pyramideA, int pyramideH) {
	System.out.print("Volumen der Pyramide betr�gt: ");
	System.out.println(pyramideA * pyramideA * pyramideH / 3 + " cm�");
	}
	public static void kugel(int kugelR, double pi) {
		System.out.print("Volumen der Kugel betr�gt: ");
		System.out.println( 4 / 3 * kugelR * kugelR * kugelR * pi + " cm�");
		
	}
}
