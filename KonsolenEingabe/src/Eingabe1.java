import java.util.Scanner;

public class Eingabe1 {

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben sie eine ganze Zahl ein: ");
		int zahl1 = myScanner.nextInt();
		
		System.out.print("Bitte geben sie eine zweite ganze Zahl ein:");
		int zahl2 = myScanner.nextInt();
		
		int ergebnis = zahl1 + zahl2;
		
		System.out.print("\n\n\nErgebnis der Adddition lautet:");
	    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);
	    
		
		myScanner.close();

	}

}
